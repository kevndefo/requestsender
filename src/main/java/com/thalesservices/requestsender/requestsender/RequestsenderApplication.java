package com.thalesservices.requestsender.requestsender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequestsenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequestsenderApplication.class, args);
	}

}
